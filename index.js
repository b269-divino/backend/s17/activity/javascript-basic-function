// console.log(" hello world");


/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

alert("Hello, User!");

function printWelcomeMessage(){
	let fullName = prompt("Please enter your full name: ");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter your location: ");
	console.log("Hello, " + fullName + "!");
	console.log("You are" + " " + age + " years old. ")
	console.log("You live in" + " " + location);
	console.log("Welcome, It's nice to meet you!");
}

printWelcomeMessage();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:


function faveBands(){
	let topFiveBands1 = "Cheese";
	let topFiveBands2 = "Greyhounds";
	let topFiveBands3 = "Razorback";
	let topFiveBands4 = "Kapatid";
	let topFiveBands5 = "Wolfgang";

	console.log("1." + topFiveBands1);
	console.log("2." + topFiveBands2);
	console.log("3." + topFiveBands3);
	console.log("4." + topFiveBands4);
	console.log("5." + topFiveBands5);
}

faveBands();




/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:


function myfaveMovies(){
	console.log("1. Clockwork Orange");
	console.log("Rotten Tomatoes Rating: 89%");
	console.log("2. Coda");
	console.log("Rotten Tomatoes Rating: 93%");
	console.log("3. Re:Born");
	console.log("Rotten Tomatoes Rating: 88%");
	console.log("4. A man named Ode");
	console.log("Rotten Tomatoes Rating: 91%");
	console.log("5. 50 first date");
	console.log("Rotten Tomatoes Rating: 78%");
}

myfaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



// function printUsers();
// 	let printFriends() = function printUsers(){
// 	alert("Hi! Please add the names of your friends.");
// 	let friend1 = alert("Enter your first friend's name:"); 
// 	let friend2 = prom("Enter your second friend's name:"); 
// 	let friend3 = prompt("Enter your third friend's name:");

// 	console.log("You are friends with:")
// 	console.log(friend1); 
// 	console.log(friend2); 
// 	// console.log(friends); -> 
// 	console.log(friend3); 
// };

// unusable codes - too many console.log request
// console.log(friend1);
// console.log(friend2);
// console.log(friend3);


function printUsers() {
  function printFriends() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:"); 
    let friend2 = prompt("Enter your second friend's name:"); 
    let friend3 = prompt("Enter your third friend's name:");
    console.log("You are friends with:");
    console.log(friend1); 
    console.log(friend2); 
    console.log(friend3); 
  }

  printFriends();
}

printUsers();

